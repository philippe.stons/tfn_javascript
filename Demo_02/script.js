document.open();
document.writeln("Bonjour!!!");
document.close();

const container = document.getElementById("text1");
container.innerHTML = "riri";
const cntr2 = document.getElementsByClassName('sub-title');
console.log(cntr2[0].innerHTML);
const span1 = document.createElement("span");
inTitle = container.appendChild(span1);
inTitle.innerHTML = " super titre";
paraphs = document.getElementsByTagName("p");
console.log(paraphs);
paraphs[1].innerHTML = "text2";
paraphs[2].innerHTML = "un super texte!!!";

spn = document.querySelector('p > span');
console.log(spn);

p_h2 = document.querySelectorAll('h2 + p');
console.log(p_h2);

const donald = document.getElementById("Clef1");

console.log(donald.id);
console.log(donald.className);
console.log(donald.innerHTML);
console.log(donald.outerHTML);
console.log(donald.attributes);

//donald.className = "demo";
donald.classList.add("demo");
console.log(donald.classList);

const elemList = document.querySelector('ul');

console.log(elemList.firstElementChild);
console.log(elemList.firstChild);

console.log(elemList.childNodes);
console.log(elemList.children);

const elemChld = elemList.firstElementChild;
console.log(elemChld);
console.log(elemChld.nextElementSibling);

console.log(elemChld.parentNode);
console.log(elemChld.parentElement);

const donaldParentUl = document.querySelector('ul > ul');
elem = document.createElement('li');
elem.id = "Clef2";
let text = "title demo";
elem.className = text; 
elem.innerHTML = "Loulou";
console.log(elem.classList);
console.log(text.split(" "));
donaldParentUl.appendChild(elem);

console.log(elemList.children[1]);

let fifi = elemList.children[1];
const fifiClone = fifi.cloneNode();
console.log(fifiClone);
const fifiCloneComplet = fifi.cloneNode(true);
console.log(fifiCloneComplet);
elemList.replaceChild(elem, fifi);

elemList.removeChild(elem);
donaldParentUl.appendChild(fifiCloneComplet);

elem2 = document.createElement('li');
elem2.id = "Clef2";
elem2.className = text; 
elem2.innerHTML = "Zaza";
//elemList.appendChild(elem2);
elemList.insertBefore(elem2, donaldParentUl);

console.log(elem2.hasAttribute("id"))
elem2.setAttribute("id", "fifi");
console.log(elem2.getAttribute("id"));
elem2.removeAttribute("id");