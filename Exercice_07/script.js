const bntSubmit = document.getElementById('btn_submit');
const bntAuto = document.getElementById('btn_auto');
const bntReset = document.getElementById('btn_reset');
const array = document.getElementById('array');

const form1 = document.form1;
const form1Elements = form1.querySelectorAll('input:not([type=button]):not([type=submit]):not([type=reset])');
console.log(form1Elements);

bntSubmit.disabled = !validForm();
// let tab =  JSON.parse(localStorage.getItem('persons'));
// tab =  tab ? tab : [];
let tab =  JSON.parse(localStorage.getItem('persons')) || [];

for(elem of tab)
{
    createRow(elem);
}

function createRow(item)
{
    let tr = document.createElement('tr');

    for(index in item)
    {
        let td = document.createElement('td');
        td.innerHTML = item[index];
        tr.appendChild(td);
    }

    array.appendChild(tr);
}

function validForm()
{
    const cp = form1.cpostal.value;
    let isValid = (cp >= 1000 && cp <= 9999);

    let index = 0;
    while(isValid && index < form1Elements.length)
    {
        if(form1Elements[index].value.trim().length === 0)
        {
            isValid = false;
        }
        index++;
    }
    return isValid;
}

form1Elements.forEach(element => {
    element.addEventListener('keyup', (e) => 
    {
        bntSubmit.disabled = !validForm();
    });
});

bntAuto.addEventListener('click', (e) => 
{
    e.preventDefault();

    form1.lastname.value = "Duck";
    form1.firstname.value = "Riri";
    form1.cpostal.value = 1234;
})

bntReset.addEventListener('click', (e) => 
{
    e.preventDefault();

    form1.reset();
})

bntSubmit.addEventListener('click', (e) => 
{
    e.preventDefault();

    let elem = {
        lastname: form1.lastname.value,
        firstname: form1.firstname.value,
        code_postal: form1.cpostal.value
    }

    tab.push(elem);
    localStorage.setItem('persons', JSON.stringify(tab));
    createRow(elem);
})