window.addEventListener('load', () => 
{
    const root = document.getElementById("root");
    const btn = document.createElement('button');
    btn.innerHTML = "click";
    btn.addEventListener("click", (e) => 
    {
        console.log(e);
        console.log("vous avez cliquer sur le bouton.");
    })

    const cptTag = document.createElement('p');
    cptTag.innerHTML = "heure";
    
    cptTag.addEventListener('mouseover', () => 
    {
        cptTag.classList.add('rouge');
    })

    cptTag.addEventListener('mouseout', () => 
    {
        cptTag.classList.remove('rouge');
    })

    root.appendChild(btn);
    root.appendChild(cptTag);
    console.log("Window loaded");
});

// window.addEventListener('beforeunload', (e) => 
// {
//     e.returnValue = "Boom";
// })