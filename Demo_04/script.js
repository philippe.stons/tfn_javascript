function hello(nom = "Donald")
{
    console.log("Hello " + nom);
}

hello();
hello("Riri");
hello("Fifi", "Duck");

// ...args
function demoArgs()
{
    console.log(arguments);
} 

demoArgs();
demoArgs(1, 2, "hello", "Zaza");

function processArgs()
{
    let result = "";
    for(const index in arguments)
    {
        const element = arguments[index];
        console.log(index);
                // if           true        false
        result += index == "0" ? element : " " + element;
        /*
        if(index == "0")
        {
            result += element;
        } else {
            result += " " + element;
        }
        */
    }
   
    console.log(result);
}
processArgs("Bonjour", "Loulou", "bienvenue!");

function demoArgs2(...args)
{
    result = args.join(" ");
    console.log(result);
}
demoArgs2();
demoArgs2("Bonjour", "Riri");

function add1(a, b)
{
    return a + b;
}

const add2 = function(a, b)
{
    return a + b; 
}

const add3 = (a, b) => 
{
    return a + b;
}

const add4 = (a, b) => a + b;

console.log(add1(5, 6));
console.log(add2(5, 6));
console.log(add3(5, 6));
console.log(add4(5, 6));

const tab = [10, 11, 1, 1000, 25, 30, 33, 22, 45, 99];
console.log(tab.sort((a, b) => a - b));

function filterTab(tab, filterFct)
{
    let res = [];
    for(const elem of tab)
    {
        if(filterFct(elem))
        {
            res.push(elem);
        }    
    }
    
    return res;
}

function oddNumber(nbr)
{
    return nbr % 2 != 0
}
console.log(filterTab(tab, oddNumber));
console.log(typeof(oddNumber));

function demoCallback(msg, cb)
{
    // Traitement ...
    const input = prompt(msg);

    if(cb && typeof(cb) == "function")
    {
        // Appel du callback
        cb(input);
    }
}

demoCallback("Entrer un nombre : ", 
    (val) => console.log(`la valeur est ${val}`));

function timerExample()
{
    console.log("Vivie le javascript!");
}

setTimeout(timerExample, 2000);
const timerid = setTimeout(timerExample, 2000);
console.log("L'id du timer est : " + timerid);
clearTimeout(timerid); // le timer sera supprimer.

const timer2 = setInterval(timerExample, 2000);

setTimeout(() => 
{
    clearInterval(timer2);
}, 10000)