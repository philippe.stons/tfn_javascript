const container = document.getElementById("root");
const tablePage = createNewElement("div");
const form = createNewElement("form");
const profile = createNewElement('div');
const thNames = ["#", "lastname", "firstname", "email", "actions"];
var array = JSON.parse(localStorage.getItem('persons')) || [];
const emailRegex = /^[a-z0-9]+(\.[a-z0-9]+)*@[a-z0-9]+\.[a-z]{1,3}$/;

class User
{
    lastname;
    firstname;
    email;

    constructor()
    {
        this.lastname = "";
        this.firstname = "";
        this.email = "";
    }

    static fillWithData(user, data)
    {
        for(let index in user)
        {
            user[index] = data[index];
        }
    }

    toString()
    {
        return this.lastname + " " + this.firstname + " " + this.email;
    }

    checkValid()
    {
        let errors = [];
        if(this.lastname == "")
        {
            errors.push(["lastname", "required"]);
        }
        if(this.firstname == "")
        {
            errors.push(["firstname", "required"]);
        }
        if(!emailRegex.test(this.email))
        {
            console.log("invalid email", emailRegex.test(this.email));
            errors.push(["email", "enter a valid email"]);
        }
        return errors;
    }
}

createForm();
createProfile();
createTable();

if(array.length > 0)
{
    personsPage();
} else 
{
    personFormPage();
}

function personFormPage(id = -1)
{
    const btn = form.querySelector('button');
    btn.personId = id;
    btn.innerHTML = id >= 0 ? "Edit" : "Create";
    if(id >= 0)
    {
        for(index in array[id])
        {
            form[index].value = array[id][index];
        }
    } else 
    {
        form.reset();
    }

    clearRootDiv();
    container.appendChild(form);
    clearErrors();
}

function createForm()
{    
    const user = new User();
    for(let index in user)
    {
        const div = createNewElement("div", { className: "form-group", id: index });
        const label = createNewElement("label", { innerHTML: index + " :" });
        label.setAttribute("for", index);
        const input = createNewElement("input", {
            // id: index,
            type: "text",
            name: index,
            value: "",
            className: "form-control",
        });
        const error = createNewElement('small', { className: "text-danger" });

        div.appendChild(label);
        div.appendChild(input);
        div.appendChild(error);
        form.appendChild(div);
    }

    const btn = createButton("Create", saveFct);

    form.appendChild(btn);
}

function clearRootDiv()
{
    while(container.firstChild)
    {
        container.removeChild(container.firstChild);
    }
}

function personsPage()
{
    clearRootDiv();
    container.appendChild(tablePage);
}

function createTable()
{
    let table = createNewElement("table", { className: "table" });
    let thead = createNewElement("thead");
    let trHead = createNewElement("tr");
    for(i = 0; i < thNames.length; i++)
    {
        let th = createNewElement("th", { scope: "col", innerHTML: thNames[i] });
        trHead.appendChild(th);
    }
    thead.appendChild(trHead);
    table.appendChild(thead);

    let tbody = createNewElement("tbody");
    for(i = 0; i < array.length; i++)
    {  
        tbody.appendChild(addUser(i));
    }
    table.appendChild(tbody);

    let btn = createButton("Create", createFct)
    
    tablePage.appendChild(table);
    tablePage.appendChild(btn);
}

function profilePage(id)
{
    h2 = profile.querySelector('h2');
    const user = new User();
    User.fillWithData(user, array[id]);
    h2.innerHTML = user.toString();

    btns = profile.querySelectorAll('button');
    for(btn of btns)
    {
        btn.personId = id;
    }
    clearRootDiv();
    container.appendChild(profile);
}

function createProfile()
{
    let h2 = createNewElement("h2");

    let div = createButtonGroup([
        { inner: "list", fct: personsFct, type: "info" },
        { inner: "edit", fct: editFct, type: "primary" },
        { inner: "delete", fct: deleteFct, type: "danger" },
    ])

    profile.appendChild(h2);
    profile.appendChild(div);
}

/*
    DOM
*/
function addUser(id)
{
    let user = array[id];

    const tr = createNewElement('tr');
    const tdId = createNewElement('td', { className: "nbr", innerHTML: id + 1 })
    tr.appendChild(tdId);

    for(index in user)
    {
        const td = createNewElement('td', { innerHTML: user[index], className: index })
        tr.appendChild(td);
    }

    const tdAction = createNewElement('td');
    const btnGrp = createButtonGroup([
        { inner: "info", fct: profileFct, type: "info", personId: id },
        { inner: "edit", fct: editFct, type: "primary", personId: id },
        { inner: "delete", fct: deleteFct, type: "danger", personId: id },
    ]) 
    tdAction.appendChild(btnGrp);
    tr.appendChild(tdAction);
    return tr;
}

function createButtonGroup(buttons)
{
    let div = createNewElement("div", { className: "btn-group", role: "group"});
    for(btn of buttons)
    {
        let btn1 = createButton(btn.inner, btn.fct, btn.type);
        btn1.personId = btn.personId;
        div.appendChild(btn1);
    }
    return div;
}

function createButton(inner, callback, type = "primary")
{
    let btn = createNewElement("button", { innerHTML: inner, type: "button", 
        className: "btn btn-"+type });
    btn.addEventListener("click", callback);

    return btn;
}

function createNewElement(tagName, options = null)
{
    let elem = document.createElement(tagName);
    if(options)
    {
        for(key in options)
        {
            elem[key] = options[key];
        }
    }

    return elem;
}

function createFct()
{
    let tr = createNewElement("tr");
    let tdId = createNewElement("td", { innerHTML: i + 1, className: 'nbr' });
    tr.appendChild(tdId);
    for(let index in array[i])
    {
        let td = createNewElement("td", { innerHTML: array[i][index], className: index });
        tr.appendChild(td);
    }
    let tdActions = createNewElement("td");
    let div = createButtonGroup([
        { inner: "profile", fct: profileFct, type: "info", personId: i },
        { inner: "edit", fct: editFct, type: "primary", personId: i },
        { inner: "delete", fct: deleteFct, type: "danger", personId: i },
    ])
    tdActions.appendChild(div);
    tr.appendChild(tdActions);
    return tr;
}

function updatePersonIds(buttons, id)
{
    for(btn of buttons)
    {
        btn.personId = id;
    }
}

/* 
!!!!!!!!!!!!!!!
    BUTTONS
!!!!!!!!!!!!!!!
*/

function personsFct(e)
{
    clearRootDiv();
    personsPage();
}

function profileFct(e)
{
    const id = e.currentTarget.personId;
    clearRootDiv();
    profilePage(id);
}

function manageErrors(errors)
{
    clearErrors();
    for(err of errors)
    {
        const small = document.forms[0].querySelector("#"+err[0] + " > small");
        small.innerHTML = err[1];
        console.log(err);
    }
}

function clearErrors()
{
    const smalls = document.forms[0].querySelectorAll("small");
    for(sml of smalls)
    {
        sml.innerHTML = "";
    }
}

function saveFct(e)
{
    const user = new User();
    for(index in user)
    {
        user[index] = document.forms[0][index].value;
    }
    console.log("user", user);
    const id = e.currentTarget.personId;

    const errors = user.checkValid();
    if(errors.length > 0)
    {
        console.log("Invalid input");
        manageErrors(errors);
        return;
    }
    const edit = !isNaN(id) && id >= 0 && id < array.length;

    if(edit)
    {
        array[id] = user;
        const tbody = tablePage.querySelector('tbody');
        const trs = tbody.querySelectorAll("tr");
        for(let index in user)
        {
            const tdLname = trs[id].querySelector('.'+index);
            tdLname.innerHTML = user[index];
        }
    } else 
    {
        array.push(user);
        const tbody = tablePage.querySelector('tbody');
        tbody.appendChild(addUser(array.length - 1));
    }
    console.log(array);
    localStorage.setItem("persons", JSON.stringify(array));
    clearRootDiv();
    personsPage();
}

function createFct(e)
{
    clearRootDiv();
    personFormPage();
}

function editFct(e)
{
    const id = e.currentTarget.personId;
    clearRootDiv();
    personFormPage(id);
}

function deleteFct(e)
{
    const id = e.currentTarget.personId;
    array.splice(id, 1);
    localStorage.setItem('persons', JSON.stringify(array));
    const tbody = tablePage.querySelector('tbody');
    const trs = tbody.querySelectorAll("tr");
    tbody.removeChild(trs[id]);
    for(let i = id + 1; i < trs.length; i++)
    {
        let btns = trs[i].querySelectorAll('button');
        let nbr = trs[i].querySelector('.nbr');
        nbr.innerHTML = i;
        updatePersonIds(btns, i - 1);
    }
    personsPage();
}