const strs = [
    "1234",
    "123abc4",
    "123a4",
    "12354",
    "abc1234",
    "abc123",
    "abcd",
    "A21C3F",
    "a123",
    "x123",
    "z123",
    "A123",
    "AA123",
    "123.2",
    "321.2 AA12 A123",
]

function testStrings(regex)
{
    console.log("REGEX : " + regex.toString());
    for(item of strs)
    {
        console.log(item + " : ", regex.test(item));
    }
}

const regEx1 = /[0-9][0-9][0-9][0-9]/
testStrings(regEx1)

const regEx2 = /^[0-9][0-9][0-9][0-9]/
testStrings(regEx2);

const regEx3 = /^[A-Z][0-9][0-9][0-9]/
testStrings(regEx3);

const regEx4 = /^[A-Z][0-9][0-9][0-9]/i 
testStrings(regEx4);

const regEx5 = /^[A-Z]*[0-9][0-9][0-9]/i 
testStrings(regEx5);

const regEx6 = /^[A-Z]+[0-9][0-9][0-9]/
testStrings(regEx6);

const regEx7 = /^[A-Z]?[0-9][0-9][0-9]/
testStrings(regEx7);

const regEx8 = /^[abcz][0-9][0-9][0-9]/
testStrings(regEx8);

const regEx9 = /[abcz][0-9][0-9][0-9]/
testStrings(regEx9);

const regEx10 = /^[abcz]+[0-9][0-9][0-9]/
testStrings(regEx10);

const regEx11 = /^[abcz]+[5-9][0-9][0-9]/
testStrings(regEx11);

const regEx12 = /^[^abcz]+[0-9][0-9][0-9]/
testStrings(regEx12);

const regEx13 = /^[^abcz]+[0-9][0-9][0-9]$/
testStrings(regEx13);

const regEx14 = /^[0-9]{3}[0-9]$/
testStrings(regEx14);

const regEx15 = /^[0-9]{3}.[0-9]$/
testStrings(regEx15);

const regEx16 = /^[0-9]{3}.*[0-9]$/
testStrings(regEx16);

const regEx17 = /^[0-9]{3}\.[0-9]$/
testStrings(regEx17);

const regEx18 = /^([0-9]{3}\.[0-9])|([a-z]{4})$/
testStrings(regEx18);

const regEx19 = /(^[0-9]{3}\.[0-9]$)|(^AA[0-9]{3}$)/
testStrings(regEx19);

const regEx21 = /(^[a-f0-9]+$)/i
testStrings(regEx21);

const regEx20 = /([0-9]{3}\.[0-9])|(AA[0-9]{2})/gi
testStrings(regEx20);
const matchs = "321.2 AA12 A123".match(regEx20);
for(const elem of matchs)
{
    console.log(elem);
}

// 10.1.2.5 192.168.1.1 0 255
const regExIp = /^(([0-9]|[0-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[0-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/gm

const ipMatches = `12.5.100.22
123.321.55.7
10.0.2.3.2
55.23.14.7
255.3.255.256`.match(regExIp);

for(elem of ipMatches)
{
    console.log(elem);
}

const emailRegex = /^[a-z0-9]+(\.[a-z0-9])*@[a-z0-9]+\.[a-z]{1,3}$/

`test@test.com
test.qwerty@test.com
admin@gmail.com
qwerty@test@be.be`