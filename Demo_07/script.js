// Local Storage

localStorage.setItem('person', "Riri");
console.log(localStorage.getItem('person'));
localStorage.removeItem('person')
console.log(localStorage.getItem('person'));

// Session Storage
sessionStorage.setItem('person', "Riri");
console.log(sessionStorage.getItem('person'));
sessionStorage.removeItem('person')
console.log(sessionStorage.getItem('person'));

const test = ["Riri", "Fifi", "Loulou"];
localStorage.setItem("children", test);

const test2 = { lastname: "Riri", firstname: "Riri" };
localStorage.setItem("child1", test2);
console.log(localStorage.getItem("child1"))
// console.log(JSON.parse(localStorage.getItem("child1")))
localStorage.setItem("child2", JSON.stringify(test2));
console.log(localStorage.getItem("child2"))
console.log(JSON.parse(localStorage.getItem("child2")))

// https://developer.mozilla.org/en-US/docs/Web/API/Document/cookie/
