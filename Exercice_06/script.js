const magasin = document.querySelectorAll("#magasin > .produit");
const panier = document.getElementById('panier');
const prix_tot = document.getElementById('prix_tot');

actualiserPrix();

for(const produit of magasin)
{
    console.log(produit);
    const btn = produit.querySelector('button');
    btn.addEventListener('click', () => 
    {
        const nom = produit.querySelector('#nom');
        const prix = produit.querySelector('#prix');
        const qty = produit.querySelector('.add_qty');
        console.log(nom.innerHTML);
        console.log(prix.innerHTML);
        addProduct({ 
            nom: nom.innerHTML, 
            prix: prix.innerHTML,
            id: produit.id,
            qty: qty.value
        });

    })

    const input_qty = produit.querySelector(".add_qty");
    input_qty.addEventListener('change', (e) => 
    {
        console.log('test');
        const val = parseInt(input_qty.value);

        if(isNaN(val) || val <= 0)
        {
            input_qty.value = 1;
        }
    })
}

function addProduct(produit)
{
    console.log(produit);
    productID = produit.id +"_panier";
    const ligne = document.getElementById(productID);
    if(!ligne)
    {
        const line = document.createElement('tr');
        line.id = productID;
        const tdNom = document.createElement('td');
        tdNom.innerHTML = produit.nom;
        const tdPrix = document.createElement('td');
        tdPrix.innerHTML = produit.prix;
        tdPrix.className = "panier_prix";
        const tdQte = document.createElement('td');
        tdQte.innerHTML = produit.qty;
        tdQte.className = "panier_qte";
        line.appendChild(tdNom);
        line.appendChild(tdPrix);
        line.appendChild(tdQte);
        panier.appendChild(line);
    } else 
    {
        const tdQte = document.querySelector("#"+productID + " .panier_qte");
        tdQte.innerHTML = parseInt(tdQte.innerText) + parseInt(produit.qty);      
    }
    actualiserPrix();
}

function actualiserPrix() 
{
    total = 0;

    for(const elem of panier.children)
    {
        const prix = parseInt(elem.querySelector(".panier_prix").innerText);
        const qty = parseInt(elem.querySelector(".panier_qte").innerText);

        total += (prix * qty);
    }
    prix_tot.value = total;
}