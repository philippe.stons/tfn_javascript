let a = ["50"];

console.log(typeof(a));
if(typeof(a) == "number")
{
    console.log("C'est un nombre!")
} else if(typeof(a) == "string")
{
    console.log("C'est une string");
} else 
{
    console.log("C'est un autre type de variable.");
    console.log(`typeof = ${typeof(a)}`);
}

let b = 12;

switch(b)
{
    case 12:
    case 22:
        console.log("1.1");
    case 32:
    case 42:
        console.log("1.2");
        break; // Bien faire attention aux breaks!!!
    case 23:
        console.log("1.3");
        break;
    case "42":
        console.log("2");
        break;
    default:
        console.log("default");
        break;
}

let i = 0;
while(i < 5)
{
    console.log(i);
    i++;
}

do
{
    console.log(i);
}while(i < 5);

for(let i = 0; i < 10; i++)
{
    console.log(i);
}

const tab = ["Riri", "Fifi", "Loulou", "Zaza"];
for(let i = 0; i < tab.length; i++)
{
    console.log(tab[i]);
}

for(let index in tab)
{
    console.log(`${typeof(index)} ${index} ${tab[index]}`);
}

for(let item of tab)
{
    console.log(item);
}

let dico = [];
dico["Riri"] = "Duck";
dico["Fifi"] = "Duck";
for(let key in dico)
{
    console.log(`${typeof(key)} ${key} ${dico[key]}`);
}

const obj = { nom: "Duck", prenom: "Riri" };
console.log(obj.nom);
console.log(obj.prenom);
for(let item in obj)
{
    console.log(`${item} value : ${obj[item]}`);
}

let elem = {
    innerHTML: "value", 
    outerHTML: "<h1 class='title' id='titre'>value</h1>",
    className: "title",
    id: "titre",
    classList: ["title"]
}

for(let item in elem)
{
    console.log(`${item} : ${elem[item]}`);
}