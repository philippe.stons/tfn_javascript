const root = document.getElementById("root");

const input = createNewElement('input', { type: "text", value: "" })
const btn = createNewElement('button', { 
    type: 'button', 
    id: "btn", 
    innerHTML: "add" 
});
const ul = createNewElement('ul');
root.appendChild(input);
root.appendChild(btn);
root.appendChild(ul);

function addElement() 
{
    if(input.value != "")
    {
        const elem = createNewElement('li', { innerText: input.value })
        ul.appendChild(elem);
        input.value = "";
        input.focus();
    }
}

btn.addEventListener('click', addElement);

input.addEventListener('keyup', (e) => 
{
    if(e.keyCode == 13)
    {
        addElement();
    }
});


function createNewElement(tagName, options = null)
{
    const elem = document.createElement(tagName);

    if(options)
    {
        for(key in options)
        {
            elem[key] = options[key];
        }
    }
    return elem;
}