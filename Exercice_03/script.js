const root = document.getElementById('root');

const date = new Date();

const jour = date.getDate();

let jourSemain = "";
switch(date.getDay())
{
    case 0:
        jourSemain = "dimanche";
        break;
    case 1:
        jourSemain = "lundi";
        break;
    case 2:
        jourSemain = "mardi";
        break;
    case 3:
        jourSemain = "mercredi";
        break;
    case 4:
        jourSemain = "jeudi";
        break;
    case 5:
        jourSemain = "vendredi";
        break;
    case 6:
        jourSemain = "samedi";
        break;
    default:
        console.error("invalid date");
        break;
}

const listOfMonths = ["jan", "feb", "mar", "avr", "mai", "jun", "jul", "aout", "sep", "oct", "nov", "dec"];
let mois = listOfMonths[date.getMonth()];

const annee = date.getFullYear();

const div = document.createElement('div');
div.innerHTML = `Nous sommes le ${jourSemain} ${jour} ${mois} ${annee}`;
root.appendChild(div);

const div2 = document.createElement('div');
div2.innerHTML = annee;
if(annee % 4 == 0 && (annee % 100 != 0 || annee % 400 == 0))
{
    div2.innerHTML += " est bissextile";
} else 
{
    div2.innerHTML += " n'est pas bissextile";
}

root.appendChild(div2);

const nbr = parseInt(window.prompt("Nombre de personnes à ajouter : "));

if(!isNaN(nbr))
{
    const ul = document.createElement('ul');
    for(let i = 0; i < nbr; i++)
    {
        let li = document.createElement('li');
        li.innerHTML = window.prompt('Nom de la personne :');
        ul.appendChild(li);
    }
    root.appendChild(ul);
}
