console.log(document.forms);
console.log(document.forms[0]);
console.log(document.forms["form1"]);
console.log(document.form1);

const f = document.forms[0];
console.log(f.elements);
console.log(f.elements[0]);
console.log(f.elements["lastname"]);
console.log(f.lastname);

console.log(f.lastname.value);
f.firstname.value = "Duck";

f.reset();
//f.submit();

// Désactiver le submit POST.

const btn = document.getElementById('send');
btn.addEventListener('click', (e) => 
{
    e.preventDefault();

    console.log(typeof(f.date.value));
    f.date.value = '2021-12-21';
})