const text = "Le cheval de Zaza Vanderquack"
// 1.1 - Retourner la position du premier « de »
console.log(text.indexOf("de"));
console.log(text.search("de"));
// 1.2 - Retourner la position du dernier « de »
console.log(text.lastIndexOf("de"));
// 2 - Indiquer l'index de « c »
console.log(text.indexOf('c'));
// 3 - Retrouver la lettre située à l'indice 21
console.log(text.charAt(21));
console.log(text[21]);
// 4 - Remplacer « cheval » par « poney »
console.log(text.replace("cheval", "poney"));
// 5 - Decouper la chaine par le délimiteur « a »
console.log(text.split("a"));
// 6 - Inverser la chaine de caractere 
console.log(text.split("")
    .reverse()
    .join(""));