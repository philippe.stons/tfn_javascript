const title = "Anton Bruckner";

const subTitles = ["Présentation", "Compétances", "Histoirique"]

const container = document.getElementById('root');

const h1 = document.createElement("h1");
h1.innerHTML = title;

container.appendChild(h1);

const h2a = document.createElement('h2');
const h2b = document.createElement('h2');
const h2c = document.createElement('h2');


h2a.innerHTML = subTitles[0]
h2b.innerHTML = subTitles[1]
h2c.innerHTML = subTitles[2]

container.appendChild(h2a)
container.appendChild(h2b)
container.appendChild(h2c)

const img = document.createElement('img');
img.src = "./tux.webp";
img.width = 200;
container.insertBefore(img, h2a);

const p1 = document.createElement("p");
const p2 = document.createElement("p");

p1.innerHTML = "J'aime les licornes!";
p2.innerHTML = 'Et les chocapic';

container.insertBefore(p1, h2b);
container.insertBefore(p2, h2b);

const tab1 = document.createElement('ul');
const tab2 = document.createElement('ul');
const ligne1 = document.createElement('li');
const ligne2 = document.createElement('li');
const ligne3 = document.createElement('li');
const ligne4 = document.createElement('li');
const ligne5 = document.createElement('li');

ligne1.innerHTML = "CH"
ligne2.innerHTML = "web"
ligne3.innerHTML = "html"
ligne4.innerHTML = "css"
ligne5.innerHTML = "javascript"

tab1.appendChild(ligne1);
tab1.appendChild(ligne2);
tab1.appendChild(tab2);
tab2.appendChild(ligne3);
tab2.appendChild(ligne4);
tab2.appendChild(ligne5);
container.insertBefore(tab1, h2c);

const def1 = document.createElement('dl');

container.appendChild(def1);
const defTitre = document.createElement('dt');
defTitre.innerHTML = "une magnifique balise dt!";
const defText1 = document.createElement("dd");
defText1.innerHTML = "C'est presque fini!";
def1.appendChild(defTitre);
def1.appendChild(defText1);