const root = document.getElementById("root");

const horloge1 = document.createElement('div');
root.appendChild(horloge1)

function showTimer()
{
    const d = new Date();

    horloge1.innerHTML = d.toLocaleTimeString();
}

setInterval(showTimer, 100);

const horloge2 = document.createElement('div');
root.appendChild(horloge2)

function showTimer2()
{
    const d = new Date();

    horloge2.innerHTML = d.toLocaleTimeString();

    setTimeout(showTimer2, 100);
}

showTimer2();